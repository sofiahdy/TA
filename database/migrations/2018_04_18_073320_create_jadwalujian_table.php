<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalujianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwalujian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_jadwal_ujian')->unique();
            $table->string('kode_fakultas');
            $table->string('kode_matakuliah');
            $table->integer('nidn')->unsigned();
            $table->date('tgl_ujian');
            $table->string('kode_thn_ajaran');
            $table->string('kode_jenis_ujian');
            $table->string('kode_kelas');
            $table->foreign('kode_fakultas')->references('kode_fakultas')->on('fakultas');
            $table->foreign('kode_matakuliah')->references('kode_matakuliah')->on('matakuliah');
            $table->foreign('nidn')->references('nidn')->on('dosen');
            $table->foreign('kode_thn_ajaran')->references('kode_thn_ajaran')->on('tahunajaran');
            $table->foreign('kode_jenis_ujian')->references('kode_jenis_ujian')->on('jenisujian');
            $table->foreign('kode_kelas')->references('kode_kelas')->on('kelas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwalujian');
    }
}
