<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurikulumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurikulum', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_kurikulum')->unique();
            $table->string('kode_fakultas');
            $table->string('kode_matakuliah');
            $table->integer('nidn')->unsigned();
            $table->string('kode_thn_ujian');
            $table->string('kode_kelas');
            $table->foreign('kode_fakultas')->references('kode_fakultas')->on('fakultas');
            $table->foreign('kode_matakuliah')->references('kode_matakuliah')->on('matakuliah');
            $table->foreign('nidn')->references('nidn')->on('dosen');
            $table->foreign('kode_thn_ajaran')->references('kode_thn_ajaran')->on('tahunajaran');
            $table->foreign('kode_kelas')->references('kode_kelas')->on('kelas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kurikulum');
    }
}
