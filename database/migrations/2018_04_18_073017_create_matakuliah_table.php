<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatakuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matakuliah', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_matakuliah')->unique();
            $table->string('matakuliah');
            $table->string('sks');
            $table->enum('semester', ['Ganjil', 'Genap']);
            $table->string('kode_prodi');
            $table->string('kode_fakultas');
            $table->integer('nidn')->unsigned();
            $table->string('kode_kelas');
            $table->foreign('kode_prodi')->references('kode_prodi')->on('prodi');
            $table->foreign('kode_fakultas')->references('kode_fakultas')->on('fakultas');
            $table->foreign('nidn')->references('nidn')->on('dosen');
            $table->foreign('kode_kelas')->references('kode_kelas')->on('kelas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matakuliah');
    }
}
