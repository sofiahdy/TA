<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisujianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenisujian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_jenis_ujian')->unique();
            $table->enum('jenis_ujian', ['UTS', 'UAS', 'Susulan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenisujian');
    }
}
