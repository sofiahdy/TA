<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArsipsoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arsipsoal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_soal')->unique();
            $table->string('kode_kurikulum');
            $table->string('kode_jenis_ujian');
            $table->string('kode_matakuliah');
            $table->integer('nidn')->unsigned();
            $table->string('kode_fakultas');
            $table->string('kode_prodi');
            $table->string('soal');
            $table->foreign('kode_kurikulum')->references('kode_kurikulum')->on('kurikulum');
            $table->foreign('kode_jenis_ujian')->references('kode_jenis_ujian')->on('jenisujian');
            $table->foreign('kode_matakuliah')->references('kode_matakuliah')->on('matakuliah');
            $table->foreign('nidn')->references('nidn')->on('dosen');
            $table->foreign('kode_fakultas')->references('kode_fakultas')->on('fakultas');
            $table->foreign('kode_prodi')->references('kode_prodi')->on('prodi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arsipsoal');
    }
}
