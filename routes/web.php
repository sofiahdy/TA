<?php
// Menuju ke halaman utama
Route::get('/', function () {
    return view('auth.login');
});

//Autentikasi 
Auth::routes();

//Menuju ke halaman utama
Route::get('/home', 'DosenController@index')->name('home');

//Route Admin
Route::get('Admin/data', 'AdminController@index');
Route::get('/Admin/add','AdminController@create');
Route::post('/Admin/store','AdminController@store');
Route::get('/Admin/edit/{id}','AdminController@edit');
Route::post('/Admin/update/{id}','AdminController@update');
Route::get('/Admin/delete/{id}','AdminController@delete');

// //Route Dosen
Route::get('/Dosen', 'DosenController@index');
Route::get('/Dosen/add','DosenController@create');
Route::post('/Dosen/store','DosenController@store');
Route::get('/Dosen/edit/{id}','DosenController@edit');
Route::post('/Dosen/update/{id}','DosenController@update');
Route::get('/Dosen/delete/{id}','DosenController@delete');

//Route Fakultas
Route::get('/Fakultas', 'FakultasController@index');
Route::get('/Fakultas/add','FakultasController@create');
Route::post('/Fakultas/store','FakultasController@store');
Route::get('/Fakultas/edit/{id}','FakultasController@edit');
Route::post('/Fakultas/update/{id}','FakultasController@update');
Route::get('/Fakultas/delete/{id}','FakultasController@delete');

//Prodi
Route::get('/Prodi', 'ProdiController@index');
Route::get('/Prodi/add','ProdiController@create');
Route::post('/Prodi/store','ProdiController@store');
Route::get('/Prodi/edit/{id}','ProdiController@edit');
Route::post('/Prodi/update/{id}','ProdiController@update');
Route::get('/Prodi/delete/{id}','ProdiController@delete');

//Matakuliah
Route::get('/Matakuliah', 'MatakuliahController@index');
Route::get('/Matakuliah/add','MatakuliahController@create');
Route::post('/Matakuliah/store','MatakuliahController@store');
Route::get('/Matakuliah/edit/{id}','MatakuliahController@edit');
Route::post('/Matakuliah/update/{id}','MatakuliahController@update');
Route::get('/Matakuliah/delete/{id}','MatakuliahController@delete');

//Kurikulum
Route::get('/Kurikulum', 'KurikulumController@index');
Route::get('/Kurikulum/add','KurikulumController@create');
Route::post('/Kurikulum/store','KurikulumController@store');
Route::get('/Kurikulum/edit/{id}','KurikulumController@edit');
Route::post('/Kurikulum/update/{id}','KurikulumController@update');
Route::get('/Kurikulum/delete/{id}','KurikulumController@delete');

//Tahun Ajaran
Route::get('/TahunAjaran', 'TahunajaranController@index');
Route::get('/TahunAjaran/add','TahunajaranController@create');
Route::post('/TahunAjaran/store','TahunajaranController@store');
Route::get('/TahunAjaran/edit/{id}','TahunajaranController@edit');
Route::post('/TahunAjaran/update/{id}','TahunajaranController@update');
Route::get('/TahunAjaran/delete/{id}','TahunajaranController@delete');

//Jenis Ujian
Route::get('/JenisUjian', 'JenisujianController@index');
Route::get('/JenisUjian/add','JenisujianController@create');
Route::post('/JenisUjian/store','JenisujianController@store');
Route::get('/JenisUjian/edit/{id}','JenisujianController@edit');
Route::post('/JenisUjian/update/{id}','JenisujianController@update');
Route::get('/JenisUjian/delete/{id}','JenisujianController@delete');

//Kelas
Route::get('/Kelas', 'KelasController@index');
Route::get('/Kelas/add','KelasController@create');
Route::post('/Kelas/store','KelasController@store');
Route::get('/Kelas/edit/{id}','KelasController@edit');
Route::post('/Kelas/update/{id}','KelasController@update');
Route::get('/Kelas/delete/{id}','KelasController@delete');

//Jadwal Ujian
Route::get('/JadwalUjian', 'JadwalujianController@index');
Route::get('/JadwalUjian/add','JadwalujianController@create');
Route::post('/JadwalUjian/store','JadwalujianController@store');
Route::get('/JadwalUjian/edit/{id}','JadwalujianController@edit');
Route::post('/JadwalUjian/update/{id}','JadwalujianController@update');
Route::get('/JadwalUjian/delete/{id}','JadwalujianController@delete');

//Nilai
Route::get('/Nilai', 'ArsipnilaiController@index');
Route::get('/Nilai/add','ArsipnilaiController@create');
Route::post('/Nilai/store','ArsipnilaiController@store');

//Soal
Route::get('/Soal', 'ArsipsoalController@index');
Route::get('/Soal/add','ArsipsoalController@create');
Route::post('/Soal/store','ArsipsoalController@store');

//Kaprodi Nilai
Route::get('/KaprodiNilai', 'ArsipnilaiController@index');
Route::get('/KaprodiNilai/edit/{id}','ArsipnilaiController@edit');
Route::post('/KaprodiNilai/update/{id}','ArsipnilaiController@update');
Route::get('/KaprodiNilai/delete/{id}','ArsipnilaiController@delete');

//Kaprodi Soal
Route::get('/KaprodiSoal', 'ArsipsoalController@index');
Route::get('/KaprodiSoal/edit/{id}','ArsipsoalController@edit');
Route::post('/KaprodiSoal/update/{id}','ArsipsoalController@update');
Route::get('/KaprodiSoal/delete/{id}','ArsipsoalController@delete');