<?php

use Illuminate\Http\Request;
//API USERS
Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');
Route::get('users', 'UserController@users');
Route::get('users/profile', 'UserController@profile')->middleware('auth:api');
Route::post('post', 'PostController@add')->middleware('auth:api');

//API DOSEN
Route::post('dosen/add', 'DosenController@adddosen')->middleware('auth:api');
Route::get('dosen', 'DosenController@dosen');
Route::get('dosen/{id}', 'DosenController@dosenById')->middleware('auth:api');
Route::put('dosen/{dosen}', 'DosenController@updatedosen')->middleware('auth:api');
Route::delete('dosen/{dosen}', 'DosenController@deletedosen')->middleware('auth:api');

//API ADMIN
Route::post('admin/add', 'AdminController@addadmin')->middleware('auth:api');
Route::get('admin', 'AdminController@admin');
Route::get('admin/{id}', 'AdminController@adminById')->middleware('auth:api');
Route::put('admin/{admin}', 'AdminController@updateadmin')->middleware('auth:api');
Route::delete('admin/{admin}', 'AdminController@deleteadmin')->middleware('auth:api');

//API JENIS UJIAN
Route::post('jenisujian/add', 'JenisujianController@addjenisujian')->middleware('auth:api');
Route::get('jenisujian', 'JenisujianController@jenisujian');
Route::get('jenisujian/{id}', 'JenisujianController@jenisujianById')->middleware('auth:api');
Route::put('jenisujian/{jenisujian}', 'JenisujianController@updatejenisujian')->middleware('auth:api');
Route::delete('jenisujian/{jenisujian}', 'JenisujianController@deletejenisujian')->middleware('auth:api');

//API TAHUN AJARAN
Route::post('tahunajaran/add', 'TahunajaranController@addtahunajaran')->middleware('auth:api');
Route::get('tahunajaran', 'TahunajaranController@tahunajaran');
Route::get('tahunajaran/{id}', 'TahunajaranController@tahunajaranById')->middleware('auth:api');
Route::put('tahunajaran/{tahunajaran}', 'TahunajaranController@updatetahunajaran')->middleware('auth:api');
Route::delete('tahunajaran/{tahunajaran}', 'TahunajaranController@deletetahunajaran')->middleware('auth:api');

//API KELAS
Route::post('kelas/add', 'KelasController@addkelas')->middleware('auth:api');
Route::get('kelas', 'KelasController@kelas');
Route::get('kelas/{id}', 'KelasController@kelasById')->middleware('auth:api');
Route::put('kelas/{kelas}', 'KelasController@updatekelas')->middleware('auth:api');
Route::delete('kelas/{kelas}', 'KelasController@deletekelas')->middleware('auth:api');

//API KURIKULUM
Route::post('kurikulum/add', 'KurikulumController@addkurikulum')->middleware('auth:api');
Route::get('kurikulum', 'KurikulumController@kurikulum');
Route::get('kurikulum/{id}', 'KurikulumController@kurikulumById')->middleware('auth:api');
Route::put('kurikulum/{kurikulum}', 'KurikulumController@updatekurikulum')->middleware('auth:api');
Route::delete('kurikulum/{kurikulum}', 'KurikulumController@deletekurikulum')->middleware('auth:api');

//API MATAKULIAH
Route::post('matakuliah/add', 'MatakuliahController@addmatakuliah')->middleware('auth:api');
Route::get('matakuliah', 'MatakuliahController@matakuliah');
Route::get('matakuliah/{id}', 'MatakuliahController@matakuliahById')->middleware('auth:api');
Route::put('matakuliah/{matakuliah}', 'MatakuliahController@updatematakuliah')->middleware('auth:api');
Route::delete('matakuliah/{matakuliah}', 'MatakuliahController@deletematakuliah')->middleware('auth:api');

//API PRODI
Route::post('prodi/add', 'ProdiController@addprodi')->middleware('auth:api');
Route::get('prodi', 'ProdiController@prodi');
Route::get('prodi/{id}', 'ProdiController@prodiById')->middleware('auth:api');
Route::put('prodi/{prodi}', 'ProdiController@updateprodi')->middleware('auth:api');
Route::delete('prodi/{prodi}', 'ProdiController@deleteprodi')->middleware('auth:api');

//API FAKULTAS
Route::post('fakultas/add', 'FakultasController@addfakultas')->middleware('auth:api');
Route::get('fakultas', 'FakultasController@fakultas');
Route::get('fakultas/{id}', 'FakultasController@fakultasById')->middleware('auth:api');
Route::put('fakultas/{fakultas}', 'FakultasController@updatefakultas')->middleware('auth:api');
Route::delete('fakultas/{fakultas}', 'FakultasController@deletefakultas')->middleware('auth:api');

//API JADWAL UJIAN
Route::post('jadwalujian/add', 'JadwalujianController@addjadwalujian')->middleware('auth:api');
Route::get('jadwalujian', 'JadwalujianController@jadwalujian');
Route::get('jadwalujian/{id}', 'JadwalujianController@jadwalujianById')->middleware('auth:api');
Route::put('jadwalujian/{jadwalujian}', 'JadwalujianController@updatejadwalujian')->middleware('auth:api');
Route::delete('jadwalujian/{jadwalujian}', 'JadwalujianController@deletejadwalujian')->middleware('auth:api');

//API ARSIP NILAI
Route::post('arsipnilai/add', 'ArsipnilaiController@addarsipnilai')->middleware('auth:api');
Route::get('arsipnilai', 'ArsipnilaiController@arsipnilai');
Route::get('arsipnilai/{id}', 'ArsipnilaiController@arsipnilaiById')->middleware('auth:api');

//API ARSIP SOAL
Route::post('arsipsoal/add', 'ArsipsoalController@addarsipsoal')->middleware('auth:api');
Route::get('arsipsoal', 'ArsipsoalController@arsipsoal');
Route::get('arsipsoal/{id}', 'ArsipsoalController@arsipsoalById')->middleware('auth:api');

//API KAPRODI NILAI
Route::get('kaprodinilai', 'ArsipnilaiController@kaprodinilai');
Route::get('kaprodinilai/{id}', 'ArsipnilaiController@kaprodinilaiById')->middleware('auth:api');
Route::put('kaprodinilai/{kaprodinilai}', 'ArsipnilaiController@updatekaprodinilai')->middleware('auth:api');
Route::delete('kaprodinilai/{kaprodinilai}', 'ArsipnilaiController@deletekaprodinilai')->middleware('auth:api');

//API KAPRODI SOAL
Route::get('kaprodisoal', 'ArsipsoalController@kaprodisoal');
Route::get('kaprodisoal/{id}', 'ArsipsoalController@kaprodisoalById')->middleware('auth:api');
Route::put('kaprodisoal/{kaprodisoal}', 'ArsipsoalController@updatekaprodisoal')->middleware('auth:api');
Route::delete('kaprodisoal/{kaprodisoal}', 'ArsipsoalController@deletekaprodisoal')->middleware('auth:api');