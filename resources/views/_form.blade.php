{{ csrf_field() }}

<div class="box box-primary">
	<div class="box-body">    
		<table>
			<tr>
				<!-- <td width="150px">Category *</td>
				<td>
					<div class="form-group">
						<input {{ request()->old('category', $newses->category) == 'Tips' ? 'checked' : '' }} required type="radio" name="category" value="Tips">Tips
						<input {{ request()->old('category', $newses->category) == 'Info' ? 'checked' : '' }} required type="radio" name="category" value="Info">Info
						<i class="text-danger">{{ $errors->first('category') }}</i>
					</div>
				</td> -->

				<td width="150px">NIDN</td>
				<td>
					<div class="form-group">
		                <input type="text" name="nidn" class="form-control" value="{{ $dosens->nidn }}" readonly>
		            </div>
	            </td>
			</tr>
			
			<tr>
				<td>Title *</td>
				<td>
					<div class="form-group">
						<input required type="text" name="title" class="form-control" placeholder="Enter ..." value="{{ request()->old('title', $newses->title) }}" />
						<i class="text-danger">{{ $errors->first('title') }}</i>
					</div>
				</td>
			</tr>
			<tr>
				<td width="150px">News *</td>
				<td>
					<div class="form-group">
						<input {{ empty($newses->id) ? 'required' : '' }} type="file" id="inputgambar" name="news" class="validate"/ >
						@if ($newses->news)
							<img src="{{ asset('image/'.$newses->news)  }}" style="height: 50px; width: 50px;" />
						@endif
						<i class="text-danger">{{ $errors->first('news') }}</i>
					</div>
				</td>
			</tr>
			<tr>
				<td>Content *</td>
				<td>
					<div class="form-group">
						<input required type="text" name="content" class="form-control" placeholder="Enter ..." value="{{ request()->old('content', $newses->content) }}" />
						<i class="text-danger">{{ $errors->first('content') }}</i>
					</div>
				</td>
			</tr>
		</table>

		<div class="clearfix form-actions">
			<div class="col-md-offset-3 col-md-9">
				<input class="btn btn-info" type="submit" class="ace-icon fa fa-check bigger-110" value="Save" id="bootbox-confirm">
				&nbsp; &nbsp; &nbsp;
				<a href="{{url('News')}}">
					<button class="btn" id="back_button" type="button">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Back
					</button>
				</a>
			</div>
		</div>
		<!-- <div class="hr hr-24"></div> -->    
	</div><!-- /.box body -->
</div>