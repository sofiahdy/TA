@extends ('layouts.index')
<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  <!-- <link rel="stylesheet" type="text/css" href="css/demo.css"> -->
  
@section ('content')
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">

<div class="col-xs-12">
  <div class="row">
    <div class="col-xs-12">
      <a href="{{url('News/add') }}">
        <button class="btn btn-xs btn btn-info">
          <i class="ace-icon fa fa-plus bigger-110"></i>
          Add
          <i class="ace-icon icon-on-right"></i>
        </button>
      </a>
      <br><br>
      <div id="datatabel_length" class="dataTables_length">
        <label>
          <select class="form-control input-sm" name="datatabel_length" aria-controls="datatabel">
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
          Records per page
        </label>
      </div>
    </div>
  </div>
  <table id="datatbl" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Category</th>
            <th>Title</th>
            <th>News</th>
            <th>Content</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
      @foreach($newses as $news)
        <tr>
          <td>{{ $news->id }}</td>
          <td>{{ $news->category }}</td>
          <td>{{ $news->title }}</td>
          <td><img src="{{ asset('image/'.$news->news)  }}" style="max-height:50px;max-width:50px;margin-top:10px;"></td>
          <td>{{ $news->content }}</td>
          <td>
            <div class="hidden-sm hidden-xs btn-group">
              <a href="{{ url('News/edit', $news->id )}}">
                <button class="btn btn-xs btn-info"><i class="ace-icon fa fa-pencil bigger-120"></i></button>
              </a>
              <!-- <a class="news_edit_modal" href="{{ url('News/edit', $news->id) }}">
                <button class="btn btn-xs btn-success"><i class="ace-icon fa fa-pencil bigger-120"></i></button>
              </a> -->
              <a class="news_delete" href="{{ url('News/delete', $news->id )}}">
                <button class="btn btn-xs btn-danger"><i class="ace-icon fa fa-trash-o bigger-120"></i></button>
              </a>
            </div>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  <!-- Modal -->
  <!-- <div id="news_modal" class="modal fade" role="dialog">
    <div class="modal-dialog"> -->

      <!-- Modal content-->
      <!-- <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          @include('News/_form', ['newses' => new \App\News])
        </div>
      </div>
    </div>
  </div> -->
</div>

<div class="col-xs-12">
  <div class="row">
    <div class="col-sm-6">
      <div id="datatabel_info" class="dataTables_info" role="alert" aria-live="polite" aria-relevant="all">Show 1 of 2</div>
    </div>
    <div class="col-sm-6">
      <div id="datatabel_paginate" class="dataTables_paginate paging_simple_numbers">
        <ul class="pagination">
          <li id="datatabel_previous" class="paginate_button previous disabled" aria-controls="datatabel" tabindex="0">
            <a href="#">Previous</a>
          </li>
          <li class="paginate_button active" aria-controls="datatabel" tabindex="0">
          <a href="#">1</a>
          </li>
          <li id="datatabel_next" class="paginate_button next disabled" aria-controls="datatabel" tabindex="0">
          <a href="#">Next</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
  @endsection

  <!-- sweet alert -->
  @push('scripts')
  <script>
    // $('.news_edit_modal').each(function () {
    //   $(this).click(function (event) {
    //     event.preventDefault();
    //     $('#news_modal').modal('show');
        // $('#news_modal').modal('hide');
        // $('#back_button').hide();

        // 1. Get data with ajax to api get
        // $.get('news/1', function ($data) {
          // 2. Set value to the form
        //   $('[name=title]').val(data.title);
        //   ...
        // });
        // 3. Save with ajax to api post / put
        // $.ajax({
        //   method: 'post',
        //   method: 'put',
        //   ...
        // })
        // 4. Close modal
    //   });
    // });

    $('.news_delete').each(function () {
      $(this).click(function (event) {
        event.preventDefault();
        var delete_url = $(this).attr('href');
        
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            window.location.href = delete_url;
          }
        });
      });
    });
  </script>
@endpush







