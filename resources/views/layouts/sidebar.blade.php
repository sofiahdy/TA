<!-- <section class="sidebar"></br> 
  <div user-panel>
    <img src="{{ asset('images/user2.png') }}" class="img-circle" width="50px" height="50px" alt="">
  </div>
  <ul class="sidebar-menu">
  
    <li class="{{ Request::segment(1) == 'News' ? 'active' : '' }}">
      <a href="News">
        <i class="fa fa-tasks"></i> <span>News</span>
      </a>
    </li>
    <li class="{{ Request::segment(1) == 'Promo' ? 'active' : '' }}">
      <a href="Promo">
        <i class="fa fa-tasks"></i> <span>Promo</span>
      </a>
    </li>
  </ul>
</section> -->
  <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ Request::segment(1) == 'Dosen' ? 'active' : '' }}">
          <a href="{{url('Dosen')}}">
            <i class="fa fa-dashboard"></i> <span>Beranda</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i>
            <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('Admin/data')}}"><i class="fa fa-circle-o"></i> Admin</a></li>
            <li><a href="{{url('Dosen')}}"><i class="fa fa-circle-o"></i> Dosen</a></li>
            <li><a href="{{url('Fakultas')}}"><i class="fa fa-circle-o"></i> Fakultas</a></li>
            <li><a href="{{url('Prodi')}}"><i class="fa fa-circle-o"></i> Prodi</a></li>
            <li><a href="{{url('Matakuliah')}}"><i class="fa fa-circle-o"></i> Matakuliah</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar"></i>
            <span>Kurikulum</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('Kurikulum')}}"><i class="fa fa-circle-o"></i> Data Kurikulum</a></li>
            <li><a href="{{url('TahunAjaran')}}"><i class="fa fa-circle-o"></i> Tahun Ajaran</a></li>
            <li><a href="{{url('JenisUjian')}}"><i class="fa fa-circle-o"></i> Jenis Ujian</a></li>
            <li><a href="{{url('Kelas')}}"><i class="fa fa-circle-o"></i> Kelas</a></li> 
          </ul>
        </li>
        <li class="{{ Request::segment(1) == 'JadwalUjian' ? 'active' : '' }}">
          <a href="{{url('JadwalUjian')}}">
            <i class="fa fa-tasks"></i> <span>Jadwal Ujian</span>
          </a>
        </li>
        <li class="{{ Request::segment(1) == 'Soal' ? 'active' : '' }}">
          <a href="{{url('Soal')}}">
            <i class="fa fa-folder"></i> <span>Soal</span>
          </a>
        </li>
        <li class="{{ Request::segment(1) == 'Nilai' ? 'active' : '' }}">
          <a href="{{url('Nilai')}}">
            <i class="fa fa-folder"></i> <span>Nilai</span>
          </a>
        </li>
        <li class="{{ Request::segment(1) == 'Pesan' ? 'active' : '' }}">
          <a href="{{url('Pesan')}}">
            <i class="fa fa-envelope"></i> <span>Pesan</span>
          </a>
        </li>
      </ul>
  </section>