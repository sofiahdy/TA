<!-- INDEX PROMO -->

@extends ('layouts.index')
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  
@section ('content')
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">


<div class="col-xs-12">
  <div class="row">
    <div class="col-xs-12">
      <a href="{{url('Promo/add') }}">
        <button class="btn btn-xs btn btn-info">
          <i class="ace-icon fa fa-plus bigger-110"></i>
          Add
          <i class="ace-icon icon-on-right"></i>
        </button>
      </a>
      <br><br>
      <div id="datatabel_length" class="dataTables_length">
        <label>
          <select class="form-control input-sm" name="datatabel_length" aria-controls="datatabel">
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
          Records per page
        </label>
      </div>
    </div>
  </div>
  <table id="datatbl" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Image</th>
            <th>Short Description</th>
            <th>Detail</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
      @foreach($promos as $promo)
        <tr>
          <td>{{ $promo->id }}</td>
          <td><img src="{{ asset('image/'.$promo->promo_image)  }}" style="max-height:50px;max-width:50px;margin-top:10px;"></td>
          <td>{{ $promo->short_description }}</td>
          <td>{{ $promo->detail }}</td>
          <td>
            <div class="hidden-sm hidden-xs btn-group">
              <a href="{{ url('Promo/edit',$promo->id )}}">
                <button class="btn btn-xs btn-info"><i class="ace-icon fa fa-pencil bigger-120"></i></button>
              </a>
              <a class="promo_delete" href="{{ url('Promo/delete', $promo->id )}}">
                <button class="btn btn-xs btn-danger"><i class="ace-icon fa fa-trash-o bigger-120"></i></button>
              </a>

            </div>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  <div id="promo_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          @include('Promo/_form', ['promos' => new \App\Promo])
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xs-12">
  <div class="row">
    <div class="col-sm-6">
      <div id="datatabel_info" class="dataTables_info" role="alert" aria-live="polite" aria-relevant="all">Show 1 of 2</div>
    </div>
    <div class="col-sm-6">
      <div id="datatabel_paginate" class="dataTables_paginate paging_simple_numbers">
        <ul class="pagination">
          <li id="datatabel_previous" class="paginate_button previous disabled" aria-controls="datatabel" tabindex="0">
            <a href="#">Previous</a>
          </li>
          <li class="paginate_button active" aria-controls="datatabel" tabindex="0">
          <a href="#">1</a>
          </li>
          <li id="datatabel_next" class="paginate_button next disabled" aria-controls="datatabel" tabindex="0">
          <a href="#">Next</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
  @endsection


@push('scripts')
  <script>
    $('.promo_delete').each(function () {
        $(this).click(function (event) {
          event.preventDefault();
          var delete_url = $(this).attr('href');
          
          swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              window.location.href = delete_url;
            }
          });
        });
      });
  </script>
@endpush