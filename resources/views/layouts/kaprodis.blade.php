<!-- <section class="sidebar"></br> 
  <div user-panel>
    <img src="{{ asset('images/user2.png') }}" class="img-circle" width="50px" height="50px" alt="">
  </div>
  <ul class="sidebar-menu">
  
    <li class="{{ Request::segment(1) == 'News' ? 'active' : '' }}">
      <a href="News">
        <i class="fa fa-tasks"></i> <span>News</span>
      </a>
    </li>
    <li class="{{ Request::segment(1) == 'Promo' ? 'active' : '' }}">
      <a href="Promo">
        <i class="fa fa-tasks"></i> <span>Promo</span>
      </a>
    </li>
  </ul>
</section> -->
<section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ Request::segment(1) == 'Dosen' ? 'active' : '' }}">
          <a href="{{url('Dosen')}}">
            <i class="fa fa-dashboard"></i> <span>Beranda</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('Soal')}}"><i class="fa fa-circle-o"></i> Data Soal</a></li>
            <li><a href="{{url('Nilai')}}"><i class="fa fa-circle-o"></i> Data Nilai</a></li>
          </ul>
        </li>
      </ul>
  </section>