{{ csrf_field() }}

<div class="box box-primary">
	<div class="box-body">    
		<table>
        <tr>
					<td width="150px">NIDN</td>
					<td>
						<div class="form-group">
		                  <input type="text" name="nidn" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Nama Dosen</td>
					<td>
						<div class="form-group">
		                  <input type="text" name="nama_dosen" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Gelar Dosen</td>
					<td>
						<div class="form-group">
		                  <input type="text" name="gelar_dosen" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
				<tr>
	                <td>Jenis Kelamin</td>
					<td>
						<div class="form-group">
		                  <input type="radio" name="jenis_kelamin" value="Perempuan">Perempuan
		                  <input type="radio" name="jenis_kelamin" value="Laki-laki">Laki-laki
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Tempat Lahir</td>
					<td>
						<div class="form-group">
		                  <input type="text" name="tempat_lahir" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Tanggal Lahir</td>
					<td>
						<div class="form-group">
		                  	<div class="input-group date">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input type="text" class="form-control pull-right" id="datepicker" name="tgl_lahir">
			                </div>
		                </div>
		            </td>
				</tr>
				<tr>
					<td>Agama</td>
					<td>
						<div class="form-group">
		                  <select name="agama">
		                  	<option value="Islam">Islam</option>
		                  	<option value="Kristen">Kristen</option>
		                  	<option value="Hindu">Hindu</option>
		                  	<option value="Katholik">Katholik</option>
		                  	<option value="Budha">Budha</option>
		                  </select>
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Telepon</td>
					<td>
						<div class="form-group">
		                  <input type="telepon" name="telepon" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
				<tr>
					<td>E-mail</td>
					<td>
						<div class="form-group">
		                  <input type="email" name="email" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>
						<div class="form-group">
		                  <input type="text" name="alamat" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>
						<div class="form-group">
		                  <input type="file" id="inputgambar" name="foto" class="validate"/ >
		                </div>
	                </td>
				</tr>
				<tr>
					<td>Kata Sandi</td>
					<td>
						<div class="form-group">
		                  <input type="text" name="password" class="form-control" placeholder="Enter ...">
		                </div>
	                </td>
				</tr>
		</table>

		<div class="clearfix form-actions">
			<div class="col-md-offset-3 col-md-9">
				<input class="btn btn-info" type="submit" class="ace-icon fa fa-check bigger-110" value="Save" id="bootbox-confirm">
				&nbsp; &nbsp; &nbsp;
				<a href="{{url('News')}}">
					<button class="btn" id="back_button" type="button">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Back
					</button>
				</a>
			</div>
		</div>
		<!-- <div class="hr hr-24"></div> -->    
	</div><!-- /.box body -->
</div>