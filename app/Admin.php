<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table='admin';
    protected $fillable = [
        'nip', 'nama_admin', 'jabatan', 'jenis_kelamin', 'tampat_lahir', 'tgl_lahir', 'agama', 'telepon', 'email', 'alamat', 'foto', 'password',
    ];
}
