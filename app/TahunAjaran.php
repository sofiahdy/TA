<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    protected $table='tahunajaran';
    protected $fillable = [
        'kode_thn_ajaran', 'tahun', 'semester',
    ];
}
