<?php
namespace App\Transformers;
use App\News;
use League\Fractal\TransformerAbstract;

class NewsTransformer extends TransformerAbstract
{
    public function transform(News $news)
    {
        return[
            'id'        =>$news->id,
            'category'  =>$news->category,
            'title'     =>$news->title,
            'news'      =>$news->news,
            'content'   =>$news->content,
        ];
    }
}