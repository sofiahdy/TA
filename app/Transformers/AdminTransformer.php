<?php

namespace App\Transformers;
use App\Admin;
use League\Fractal\TransformerAbstract;

class AdminTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Admin $admin)
    {
        return [
            'id'    =>$admin->id,
            'nip'  =>$admin->nidn,
            'nama_admin'     =>$admin->nama_admin,
            'jabatan'      =>$admin->gelar_admin,
            'jenis_kelamin'   =>$admin->jenis_kelamin,
            'tempat_lahir'  =>$admin->tempat_lahir,
            'tgl_lahir'     =>$admin->tgl_lahir,
            'agama'      =>$admin->agama,
            'email'   =>$admin->email,
            'alamat'  =>$admin->alamat,
            'telepon'     =>$admin->telepon,
            'foto'      =>$admin->foto,
        ];
    }
}
