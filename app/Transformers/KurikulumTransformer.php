<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Kurikulum;

class KurikulumTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Kurikulum $kurikulum)
    {
        return [
            'id'    =>$kurikulum->id,
            'kode_kurikulum'  =>$kurikulum->kode_kurikulum,
            'kode_matakuliah'     =>$kurikulum->kode_matakuliah,
            'nidn'      =>$kurikulum->nidn,
            'kode_thn_ajaran'   =>$kurikulum->kode_thn_ajaran,
            'kode_kelas'  =>$kurikulum->kode_kelas,
        ];
    }
}
