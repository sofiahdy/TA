<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Kelas;

class KelasTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Kelas $kelas)
    {
        return [
            'id'    =>$kelas->id,
            'kode_kelas'  =>$kelas->kode_kelas,
            'kelas'     =>$kelas->kelas,
        ];
    }
}
