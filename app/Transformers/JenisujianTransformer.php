<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\JenisUjian;

class JenisujianTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(JenisUjian $jenisujian)
    {
        return [
            'id'    =>$jenisujian->id,
            'kode_jenis_ujian'  =>$jenisujian->kode_jenis_ujian,
            'jenis_ujian'     =>$jenisujian->jenis_ujian,
        ];
    }
}
