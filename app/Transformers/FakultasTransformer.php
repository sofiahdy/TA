<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Fakultas;

class FakultasTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Fakultas $fakultas)
    {
        return [
            'id'    =>$fakultas->id,
            'kode_fakultas'  =>$fakultas->kode_fakultas,
            'fakultas'     =>$fakultas->fakultas,
            'nidn'      =>$fakultas->nidn,
        ];
    }
}
