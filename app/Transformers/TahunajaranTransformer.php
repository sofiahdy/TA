<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\TahunAjaran;

class TahunajaranTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TahunAjaran $thnajaran)
    {
        return [
            'id'    =>$thnajaran->id,
            'kode_thn_ajaran'  =>$thnajaran->kode_thn_ajaran,
            'tahun'     =>$thnajaran->tahun,
            'semester'      =>$thnajaran->semester,
        ];
    }
}
