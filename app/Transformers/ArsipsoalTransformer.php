<?php

namespace App\Transformers;
use App\ArsipSoal;
use League\Fractal\TransformerAbstract;

class ArsipsoalTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ArsipSoal $soal)
    {
        return [
            'kode_soal'  =>$soal->kode_soal,
            'kode_kurikulum'     =>$soal->kode_kurikulum,
            'kode_jenis_ujian'  =>$soal->kode_jenis_ujian,
            'kode_matakuliah'      =>$soal->kode_matakuliah,
            'nidn'      =>$soal->nidn,
            'kode_fakultas'      =>$soal->kode_fakultas,
            'kode_prodi'      =>$soal->kode_prodi,
            'soal'      =>$soal->soal,
        ];
    }
}
