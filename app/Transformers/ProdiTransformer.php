<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Prodi;

class ProdiTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Prodi $prodi)
    {
        return [
            'id'    =>$prodi->id,
            'kode_prodi'  =>$prodi->kode_prodi,
            'prodi'     =>$prodi->prodi,
            'kode_fakultas'      =>$prodi->kode_fakultas,
            'nidn'   =>$prodi->nidn,
        ];
    }
}
