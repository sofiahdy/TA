<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Matakuliah;

class MatakuliahTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Matakuliah $matakuliah)
    {
        return [
            'id'    =>$prodi->id,
            'kode_matakuliah'  =>$matakuliah->kode_matakuliah,
            'matakuliah'     =>$matakuliah->matakuliah,
            'sks'      =>$matakuliah->sks,
            'semester'   =>$matakuliah->semester,
            'kode_prodi'  =>$matakuliah->kode_prodi,
            'kode_fakultas'     =>$matakuliah->kode_fakultas,
            'nidn'      =>$matakuliah->nidn,
            'kode_kelas'    =>$matakuliah->kode_kelas,
        ];
    }
}
