<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\JadwalUjian;

class JadwalujianTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(JadwalUjian $jadwalujian)
    {
        return [
            'kode_jadwal_ujian'  =>$jadwalujian->kode_jadwal_ujian,
            'kode_fakultas'  =>$jadwalujian->kode_fakultas,
            'kode_matakuliah'     =>$jadwalujian->kode_matakuliah,
            'nidn'      =>$jadwalujian->nidn,
            'kode_thn_ajaran'  =>$jadwalujian->kode_thn_ajaran,
            'kode_jenis_ujian'  =>$jadwalujian->kode_jenis_ujian,
            'kode_kelas'  =>$jadwalujian->kode_kelas,
        ];
    }
}
