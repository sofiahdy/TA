<?php

namespace App\Transformers;
use App\Dosen;
use League\Fractal\TransformerAbstract;

class DosenTransformer extends TransformerAbstract
{
    public function transform(Dosen $dosen)
    {
        return[
            'nidn'  =>$dosen->nidn,
            'nama_dosen'     =>$dosen->nama_dosen,
            'gelar_dosen'      =>$dosen->gelar_dosen,
            'jenis_kelamin'   =>$dosen->jenis_kelamin,
            'tempat_lahir'  =>$dosen->tempat_lahir,
            'tgl_lahir'     =>$dosen->tgl_lahir,
            'agama'      =>$dosen->agama,
            'email'   =>$dosen->email,
            'alamat'  =>$dosen->alamat,
            'telepon'     =>$dosen->telepon,
            'foto'      =>$dosen->foto,
        ];
    }
}
