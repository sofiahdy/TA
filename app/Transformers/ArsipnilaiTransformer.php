<?php

namespace App\Transformers;
use APP\ArsipNilai;
use League\Fractal\TransformerAbstract;

class ArsipnilaiTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ArsipNilai $nilai)
    {
        return [
            'kode_nilai'  =>$nilai->kode_nilai,
            'kode_kurikulum'     =>$nilai->kode_kurikulum,
            'kode_jenis_ujian'  =>$nilai->kode_jenis_ujian,
            'kode_matakuliah'      =>$nilai->kode_matakuliah,
            'nidn'      =>$nilai->nidn,
            'kode_fakultas'      =>$nilai->kode_fakultas,
            'kode_prodi'      =>$nilai->kode_prodi,
            'nilai'      =>$nilai->nilai,
        ];
    }
}
