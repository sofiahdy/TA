<?php
namespace App\Transformers;

use App\Promo;
use League\Fractal\TransformerAbstract;

class PromoTransformer extends TransformerAbstract
{
    public function transform(Promo $promo)
    {
        return[
            'id'                    =>$promo->id,
            'promo_image'           =>$promo->promo_image,
            'short_description'     =>$promo->short_description,
            'detail'                =>$promo->detail,
        ];
    }
}