<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalUjian extends Model
{
    protected $table='jadwalujian';
    protected $fillable = [
        'kode_jadwal_ujian', 'kode_fakultas', 'kode_matakuliah', 'nidn', 'tgl_ujian', 'kode_thn_ajaran', 'kode_jenis_ujian', 'kode_kelas',
    ];
}
