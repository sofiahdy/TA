<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table='dosen';
    protected $fillable = [
        'nidn', 'nama_dosen', 'gelar_dosen', 'jenis_kelamin', 'tempat_lahir', 'tgl_lahir', 'agama', 'telepon', 'email', 'alamat', 'foto', 'password',
    ];
}
