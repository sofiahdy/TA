<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisUjian extends Model
{
    protected $table='jenisujian';
    protected $fillable = [
        'kode_jenis_ujian', 'jenis_ujian',
    ];
}
