<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArsipNilai extends Model
{
    protected $table='nilai';
    protected $fillable = [
        'kode_nilai', 'kode_kurikulum', 'kode_jenis_ujian', 'kode_matakuliah', 'nidn', 'kode_fakultas', 'kode_prodi', 'nilai',
    ];
}
