<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
    protected $table='matakuliah';
    protected $fillable = [
        'kode_matakuliah', 'matakuliah', 'sks', 'semester', 'kode_prodi', 'kode_fakultas', 'nidn', 'kode_kelas',
    ];
}
