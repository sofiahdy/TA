<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArsipSoal;
use App\Transformers\ArsipsoalTransformer;

class ArsipsoalController extends Controller
{
    public function index(){
    	$arsipsoals = ArsipSoal::all();

    	return view ('ArsipSoal.index',compact('arsipsoals'));
    }

    public function create()
    {
        return view('ArsipSoal.add');
    }

    public function store(Request $request)
    {
        $arsipsoals = new ArsipSoal();
            $arsipsoals->kode_soal = $request->kode_soal;
            $arsipsoals->kode_kurikulum = $request->kode_kurikulum;
            $arsipsoals->kode_jenis_ujian = $request->kode_jenis_ujian;
            $arsipsoals->kode_matakuliah = $request->kode_matakuliah;
            $arsipsoals->nidn = $request->nidn;
            $arsipsoals->kode_fakultas = $request->kode_fakultas;
            $arsipsoals->kode_prodi = $request->kode_prodi;

            $file       = $request->file('foto');
            $fileName   = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $arsipsoals->soal = $fileName;
    
        if($arsipsoals->save())
            return redirect('/ArsipSoal');
    }

    //API
    public function addsoal(Request $request, ArsipSoal $soal)
    {
        $this->validate($request, [
            'kode_soal'  =>'required',
            'kode_kurikulum'     =>'required',
            'kode_jenis_ujian'  =>'required',
            'kode_matakuliah'      =>'required',
            'nidn'      =>'required',
            'kode_fakultas'      =>'required',
            'kode_prodi'      =>'required',
            'soal'      =>'required',
        ]);

        $soal = $soal->create([
            'kode_soal'  =>$request->kode_soal,
            'kode_kurikulum'     =>$request->kode_kurikulum,
            'kode_jenis_ujian'  =>$request->kode_jenis_ujian,
            'kode_matakuliah'      =>$request->kode_matakuliah,
            'nidn'      =>$request->nidn,
            'kode_fakultas'      =>$request->kode_fakultas,
            'kode_prodi'      =>$request->kode_prodi,
            'soal'      =>$request->soal,
        ]);

        $response = fractal()
            ->item($soal)
            ->transformWith(new ArsipsoalTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
