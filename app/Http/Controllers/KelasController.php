<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;
use App\Transformers\KelasTransformer;

class KelasController extends Controller
{
    public function index(){
    	$kelass = Kelas::all();

    	return view ('Kelas.index',compact('kelass'));
    }

    public function create()
    {
        return view('Kelas.add');
    }

    public function store(Request $request)
    {
        $kelass = new Kelas();
            $kelass->kode_kelas = $request->kode_kelas;
            $kelass->kelas = $request->kelas;
    
            if($kelass->save())
                return redirect('/Kelas');
    }

    public function edit($id)
    {
        $kelass = Kelas::find($id);
        return view('Kelas.update', compact('kelass'));

    }

    public function update(Request $request,$id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->fill($request->input());
        
        $kelas->update();
        return redirect('/Kelas');
    }

    public function delete($id)
    {
        if($kelass = Kelas::find($id)){
            $kelass->delete($id);
                return redirect('/Kelas');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function addkelas(Request $request, Kelas $kelas)
    {
        $this->validate($request, [
            'kode_kelas'  =>'required',
            'kelas'     =>'required',
        ]);

        $kelas = $kelas->create([
            'kode_kelas'  =>$request->kode_kelas,
            'kelas'     =>$request->kelas,
        ]);

        $response = fractal()
            ->item($kelas)
            ->transformWith(new KelasTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
