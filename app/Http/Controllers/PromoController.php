<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promo;
use App\Transformers\PromoTransformer;

class PromoController extends Controller
{
    public function index(){
    	$promos = Promo::all();
    	return view ('Promo.index',compact('promos'));
    }

    public function create()
    {
        $data['promos'] = new Promo;
        return view('Promo.add', $data);
        // return view('Promo.add');
    }

    public function store(Request $request)
    // public function store(\App\Http\Requests\Promo\StoreRequest $request)
    {
        $promos = new Promo();
            $promos->id                = $request->id;
            $file                      = $request->file('image');
            $fileName                  = $file->getClientOriginalName();
            $request->file('image')->move("image/", $fileName);
            $promos->promo_image       = $fileName;
            
            $promos->short_description = $request->short_description;
            $promos->detail            = $request->detail;
    
        if($promos->save())
            return redirect('/Promo');
    
    }
    public function edit($id)
    {

        // $promos = Promo::findOrFail($id);
        // return view('Promo.update', compact('promos'));
        if($promos = Promo::find($id))
        {
            return view('Promo.update', compact('promos'));
        }
            else
        {
            return "Data not found";
        }
    
    }

    public function update(Request $request,$id)
    {
        $promos = Promo::findOrFail($id);
        // $promos ->id    = $request->id;
        $promos->fill($request->input());

        if($request->hasFile('image'))
        {
            $file                      = $request->file('image');
            $fileName                  = $file->getClientOriginalName();
            $request->file('image')->move("image/", $fileName);

            $promos->promo_image         = $fileName;

            $promos->short_description = $request->short_description;
            $promos->detail            = $request->detail;
        }

        $promos->update();
        return redirect('/Promo');
        // if($promos->update())
        // {
        //     return redirect('/Promo');
    
        // }else{
        //     return "Data not found";
        // }
    }

    public function delete($id)
    {
        if($promos = Promo::find($id)){
            $promos->delete($id);
                return redirect('/Promo');
        }else{
            return "Failed to deleted";
        }
    }

    public function addpromo(Request $request, Promo $promo)
    {
        $this->validate($request, [
            'promo_image'           =>'required',
            'short_description'     =>'required',
            'detail'                =>'required',
        ]);

        $promo = $promo->create([
            'promo_image'            =>$request->promo_image,
            'short_description'      =>$request->short_description,
            'detail'                 =>$request->detail,
        ]);

        $response = fractal()
            ->item($promo)
            ->transformWith(new PromoTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function promo(Promo $promo)
    {
        $promos = $promo->paginate(5);

        return fractal()
            ->collection($promos)
            ->transformWith(new PromoTransformer)
            ->toArray();
    }

    public function updatepromo(Request $request, Promo $promo)
    {
        if ($promos = Promo::find($promo)) {
            $promo->promo_image        = $request->get('promo_image', $promo->promo_image);
            $promo->short_description  = $request->get('short_description', $promo->short_description);
            $promo->detail             = $request->get('detail', $promo->detail);
            $promo->save();
            $response = fractal()
                ->item($promo)
                ->transformWith(new PromoTransformer)
                ->toArray();
        return response()->json($response);
        }else{
            return response()->json([
                'Message' => 'Failed to update',
            ]);
        }
        
    }

    public function deletepromo(Promo $promo)
    {
        if($promo->delete())
        {
            return response()->json([
                'Message' => 'Promo deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete. Please check id',
            ]);
        }
        
    }
}
