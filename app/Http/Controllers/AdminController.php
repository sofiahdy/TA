<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Transformers\AdminTransformer;

class AdminController extends Controller
{
    public function index(){

    	$admins = Admin::all();

        return view ('Admin.index',compact('admins'));
        // return "ada";
    }

    public function create()
    {
        return view('Admin.add');
    }

    public function store(Request $request)
    {
        $admins = new Admin();
            $admins->nip = $request->nip;
            $admins->nama_admin = $request->nama_admin;
            $admins->jabatan = $request->jabatan;
            $admins->jenis_kelamin = $request->jenis_kelamin;
            $admins->tempat_lahir = $request->tempat_lahir;
            $admins->tgl_lahir = $request->tgl_lahir;
            $admins->agama = $request->agama;
            $admins->telepon = $request->telepon;
            $admins->email = $request->email;
            $admins->alamat = $request->alamat;
            
            $file       = $request->file('foto');
            $fileName   = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $admins->foto = $fileName;
            $admins->password = $request->password;
    
        if($admins->save())
            return redirect('/Admin/data');
    }

    public function edit($id)
    {
        $admins = Admin::find($id);
        return view('Admin.update', compact('admins'));

    }

    public function update(Request $request,$id)
    {
        $admin = Admin::findOrFail($id);
        $admin->fill($request->input());
        
        if($request->hasFile('foto'))
        {
            $file           = $request->file('foto');
            $fileName       = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $admin->foto     = $fileName;
            $admin->password  = $request->password;
        }
        $admin->update();
        return redirect('/Admin/data');
    }

    public function delete($id)
    {
        if($admins = Admin::find($id)){
            $admins->delete($id);
                return redirect('/Admin/data');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function addadmin(Request $request, Admin $admin)
    {
        $this->validate($request, [
            'nip'  =>'required',
            'nama_admin'     =>'required',
            'jabatan'      =>'required',
            'jenis_kelamin'   =>'required',
            'tempat_lahir'  =>'required',
            'tgl_lahir'     =>'required',
            'agama'      =>'required',
            'telepon'   =>'required',
            'alamat'  =>'required',
            'email'     =>'required',
            'foto'      =>'required',
            'password'   =>'required',
        ]);

        $admin = $admin->create([
            'nip'  =>$request->nidn,
            'nama_admin'     =>$request->nama_dosen,
            'jabatan'      =>$request->gelar_dosen,
            'jenis_kelamin'   =>$request->jenis_kelamin,
            'tempat_lahir'  =>$request->tempat_lahir,
            'tgl_lahir'     =>$request->tgl_lahir,
            'agama'      =>$request->agama,
            'telepon'     =>$request->telepon,
            'email'   =>$request->email,
            'alamat'  =>$request->alamat,
            'foto'      =>$request->foto,
            'password'   =>$request->password,
        ]);

        $response = fractal()
            ->item($admin)
            ->transformWith(new AdminTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
