<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisUjian;
use App\Transformers\JenisujianTransformer;

class JenisujianController extends Controller
{
    public function index(){
    	$jenisujians = JenisUjian::all();

    	return view ('JenisUjian.index',compact('jenisujians'));
    }

    public function create()
    {
        return view('JenisUjian.add');
    }

    public function store(Request $request)
    {
        $jenisujians = new JenisUjian();
            $jenisujians->kode_jenis_ujian = $request->kode_jenis_ujian;
            $jenisujians->jenis_ujian = $request->jenis_ujian;
    
        if($jenisujians->save())
            return redirect('/JenisUjian');
    }

    public function edit($id)
    {
        $jenisujians = JenisUjian::find($id);
        return view('JenisUjian.update', compact('jenisujians'));

    }

    public function update(Request $request,$id)
    {
        $jenisujian = JenisUjian::findOrFail($id);
        $jenisujian->fill($request->input());
        $jenisujian->update();
        return redirect('/JenisUjian');
    }

    public function delete($id)
    {
        if($jenisujians = JenisUjian::find($id)){
            $jenisujians->delete($id);
                return redirect('/JenisUjian');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function addjenisujian(Request $request, JenisUjian $jenisujian)
    {
        $this->validate($request, [
            'kode_jenis_ujian'  =>'required',
            'jenis_ujian'     =>'required',
        ]);

        $jenisujian = $jenisujian->create([
            'kode_jenis_ujian'  =>$request->kode_jenis_ujian,
            'jenis_ujian'     =>$request->jenis_ujian,
        ]);

        $response = fractal()
            ->item($jenisujian)
            ->transformWith(new JenisujianTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
