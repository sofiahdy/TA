<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Matakuliah;
use App\Transformers\MatakuliahTransformer;

class MatakuliahController extends Controller
{
    public function index(){
    	$matakuliahs = Matakuliah::all();

    	return view ('Matakuliah.index',compact('matakuliahs'));
    }

    public function create()
    {
        return view('Matakuliah.add');
    }

    public function store(Request $request)
    {
        $matakuliahs = new Matakuliah();
            $matakuliahs->kode_matakuliah = $request->kode_matakuliah;
            $matakuliahs->matakuliah = $request->matakuliah;
            $matakuliahs->sks = $request->sks;
            $matakuliahs->semester = $request->semester;
            $matakuliahs->kode_prodi = $request->kode_prodi;
            $matakuliahs->kode_fakultas = $request->kode_fakultas;
            $matakuliahs->nidn = $request->nidn;
            $matakuliahs->kode_kelas = $request->kode_kelas;
    
        if($matakuliahs->save())
            return redirect('/Matakuliah');
    }

    public function edit($id)
    {
        $matakuliahs = Matakuliah::find($id);
        return view('Matakuliah.update', compact('matakuliahs'));

    }

    public function update(Request $request,$id)
    {
        $matakuliah = Matakuliah::findOrFail($id);
        $matakuliah->fill($request->input());
        
        $matakuliah->update();
        return redirect('/Matakuliah');
    }

    public function delete($id)
    {
        if($matakuliahs = Matakuliah::find($id)){
            $matakuliahs->delete($id);
                return redirect('/Matakuliah');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function addmatakuliah(Request $request, Matakuliah $matkul)
    {
        $this->validate($request, [
            'kode_matakuliah'  =>'required',
            'matakuliah'     =>'required',
            'sks'  =>'required',
            'semester'      =>'required',
            'kode_prodi'  =>'required',
            'kode_fakultas'     =>'required',
            'nidn'  =>'required',
            'kode_kelas'      =>'required',
        ]);

        $matkul = $matkul->create([
            'kode_matakuliah'  =>$request->kode_matakuliah,
            'matakuliah'     =>$request->matakuliah,
            'sks'  =>$request->sks,
            'semester'      =>$request->semester,
            'kode_prodi'  =>$request->kode_prodi,
            'kode_fakultas'     =>$request->kode_fakultas,
            'nidn'  =>$request->nidn,
            'kode_kelas'      =>$request->kode_kelas,
        ]);

        $response = fractal()
            ->item($matkul)
            ->transformWith(new MatakuliahTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
