<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JadwalUjian;
use App\Transformers\JadwalujianTransformer;

class JadwalujianController extends Controller
{
    public function index(){
    	$jadwalujians = JadwalUjian::all();

    	return view ('JadwalUjian.index',compact('jadwalujians'));
    }

    public function create()
    {
        return view('JadwalUjian.add');
    }

    public function store(Request $request)
    {
        $jadwalujians = new JadwalUjian();
            $jadwalujians->kode_jadwal_ujian = $request->kode_jadwal_ujian;
            $jadwalujians->kode_fakultas = $request->kode_fakultas;
            $jadwalujians->kode_matakuliah = $request->kode_matakuliah;
            $jadwalujians->nidn = $request->nidn;
            $jadwalujians->kode_thn_ajaran = $request->kode_thn_ajaran;
            $jadwalujians->kode_jenis_ujian = $request->kode_jenis_ujian;
            $jadwalujians->kode_kelas = $request->kode_kelas;
    
        if($jadwalujians->save())
            return redirect('/JadwalUjian');
    }

    public function edit($id)
    {
        $jadwalujians = JadwalUjian::find($id);
        return view('JadwalUjian.update', compact('jadwalujians'));

    }

    public function update(Request $request,$id)
    {
        $jadwalujian = JadwalUjian::findOrFail($id);
        $jadwalujian->fill($request->input());
        
        $jadwalujian->update();
        return redirect('/JadwalUjian');
    }

    public function delete($id)
    {
        if($jadwalujians = JadwalUjian::find($id)){
            $jadwalujians->delete($id);
                return redirect('/JadwalUjian');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function addjadwalujian(Request $request, JadwalUjian $jadwalujian)
    {
        $this->validate($request, [
            'kode_jadwal_ujian'  =>'required',
            'kode_fakultas'  =>'required',
            'kode_matakuliah'     =>'required',
            'nidn'      =>'required',
            'kode_thn_ajaran'  =>'required',
            'kode_jenis_ujian'  =>'required',
            'kode_kelas'  =>'required',
        ]);

        $jadwalujian = $jadwalujian->create([
            'kode_jadwal_ujian'  =>$request->kode_jadwal_ujian,
            'kode_fakultas'  =>$request->kode_fakultas,
            'kode_matakuliah'     =>$request->kode_matakuliah,
            'nidn'      =>$request->nidn,
            'kode_thn_ajaran'  =>$request->kode_thn_ajaran,
            'kode_jenis_ujian'  =>$request->kode_jenis_ujian,
            'kode_kelas'  =>$request->kode_kelas,
        ]);

        $response = fractal()
            ->item($jadwalujian)
            ->transformWith(new JadwalujianTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
