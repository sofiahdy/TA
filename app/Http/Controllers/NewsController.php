<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Transformers\NewsTransformer;

class NewsController extends Controller
{
    public function index(){
        $newses = News::all();
        return view ('News.index', compact('newses'));
    }

    public function create()
    {
        $data['newses'] = new News;
        return view('News.add', $data);
    }

    public function store(\App\Http\Requests\News\StoreRequest $request)
    {
        $newses = new News();
            $newses->id = $request->id;
            $newses->category = $request->category;
            $newses->title = $request->title;
            
            $file       = $request->file('news');
            $fileName   = $file->getClientOriginalName();
            $request->file('news')->move("image/", $fileName);
            $newses->news = $fileName;

            $newses->content = $request->content;
    
        if($newses->save())
            return redirect('/News');
    
    }

    public function edit($id)
    {
        $newses = News::findOrFail($id);
        return view('News.update', compact('newses'));
    }

    public function update(\App\Http\Requests\News\UpdateRequest $request, $id)
    {
        $news = News::findOrFail($id);
        $news->fill($request->input());
        
        if($request->hasFile('news'))
        {
            $file           = $request->file('news');
            $fileName       = $file->getClientOriginalName();
            $request->file('news')->move("image/", $fileName);
            $news->news     = $fileName;
            $news->content  = $request->content;
        }

        $news->update();
        return redirect('/News');
    }

    public function delete($id)
    {
        if($newses = News::find($id)){
            $newses->delete($id);
                return redirect('/News');
        }else{
            return "Failed to deleted";
        }
    }

    public function addnews(Request $request, News $news)
    {
        $this->validate($request, [
            'category'  =>'required',
            'title'     =>'required',
            'news'      =>'required',
            'content'   =>'required',
        ]);

        $news = $news->create([
            'category'  =>$request->category,
            'title'     =>$request->title,
            'news'      =>$request->news,
            'content'   =>$request->content,
        ]);

        $response = fractal()
            ->item($news)
            ->transformWith(new NewsTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function updatenews(Request $request, News $news)
    {
        if ($newses = News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            $response = fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            return response()->json($response);
        }else{
            return response()->json([
                'Message' => 'Failed to update',
            ]);
        }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }

}
