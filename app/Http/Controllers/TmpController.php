<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;

class DosenController extends Controller
{
    public function index(){
    	$dosens = Dosen::all();

    	return view ('Dosen.index',compact('dosens'));
    }

    public function create()
    {
        return view('Dosen.add');
    }

    public function store(Request $request)
    {
        $dosens = new Dosen();
            $dosens->nidn = $request->nidn;
            $dosens->nama_dosen = $request->nama_dosen;
            $dosens->gelar_dosen = $request->gelar_dosen;
            $dosens->jenis_kelamin = $request->jenis_kelamin;
            $dosens->tempat_lahir = $request->tempat_lahir;
            $dosens->tgl_lahir = $request->tgl_lahir;
            $dosens->agama = $request->agama;
            $dosens->telepon = $request->telepon;
            $dosens->email = $request->email;
            $dosens->alamat = $request->alamat;

            $file       = $request->file('foto');
            $fileName   = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $dosens->foto = $fileName;

            $dosens->password = $request->password;
    
        if($dosens->save())
            return redirect('/Dosen');
    }

    public function edit($id)
    {
        $dosens = Dosen::find($id);
        return view('Dosen.update', compact('dosens'));

    }

    public function update(Request $request,$id)
    {
        $dosen = Dosen::findOrFail($id);
        $dosen->fill($request->input());
        
        if($request->hasFile('foto'))
        {
            $file           = $request->file('foto');
            $fileName       = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $dosen->foto     = $fileName;
            $dosen->password  = $request->password;
        }
        $dosen->update();
        return redirect('/Dosen');
    }

    public function delete($id)
    {
        if($dosens = Dosen::find($id)){
            $dosens->delete($id);
                return redirect('/Dosen');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function adddosen(Request $request, Dosen $dosen)
    {
        $this->validate($request, [
            'nidn'  =>'required',
            'nama_dosen'     =>'required',
            'gelar_dosen'      =>'required',
            'jenis_kelamin'   =>'required',
            'tampat_lahir'  =>'required',
            'tgl_lahir'     =>'required',
            'agama'      =>'required',
            'telepon'   =>'required',
            'alamat'  =>'required',
            'email'     =>'required',
            'foto'      =>'required',
            'password'   =>'required',
        ]);

        $dosen = $dosen->create([
            'nidn'  =>$request->nidn,
            'nama_dosen'     =>$request->nama_dosen,
            'gelar_dosen'      =>$request->gelar_dosen,
            'jenis_kelamin'   =>$request->jenis_kelamin,
            'tampat_lahir'  =>$request->tampat_lahir,
            'tgl_lahir'     =>$request->tgl_lahir,
            'agama'      =>$request->agama,
            'email'   =>$request->email,
            'alamat'  =>$request->alamat,
            'telepon'     =>$request->telepon,
            'foto'      =>$request->foto,
            'password'   =>$request->password,
        ]);

        $response = fractal()
            ->item($dosen)
            ->transformWith(new DosenTransformer)
            ->toArray();
        return response()->json($response, 201);
    }
}
