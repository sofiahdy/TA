<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\Transformers\DosenTransformer;

class DosenController extends Controller
{
    public function index(){
    	$dosens = Dosen::all();

    	return view ('Dosen.index',compact('dosens'));
    }

    public function create()
    {
        return view('Dosen.add');
    }

    public function store(Request $request)
    {
        $dosens = new Dosen();
            $dosens->nidn = $request->nidn;
            $dosens->nama_dosen = $request->nama_dosen;
            $dosens->gelar_dosen = $request->gelar_dosen;
            $dosens->jenis_kelamin = $request->jenis_kelamin;
            $dosens->tempat_lahir = $request->tempat_lahir;
            $dosens->tgl_lahir = $request->tgl_lahir;
            $dosens->agama = $request->agama;
            $dosens->telepon = $request->telepon;
            $dosens->email = $request->email;
            $dosens->alamat = $request->alamat;

            $file       = $request->file('foto');
            $fileName   = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $dosens->foto = $fileName;

            $dosens->password = $request->password;
    
        if($dosens->save())
            return redirect('/Dosen');
    }

    public function edit($id)
    {
        $dosens = Dosen::find($id);
        return view('Dosen.update', compact('dosens'));

    }

    public function update(Request $request,$id)
    {
        $dosen = Dosen::findOrFail($id);
        $dosen->fill($request->input());
        
        if($request->hasFile('foto'))
        {
            $file           = $request->file('foto');
            $fileName       = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $dosen->foto     = $fileName;
            $dosen->password  = $request->password;
        }
        $dosen->update();
        return redirect('/Dosen');
    }

    public function delete($id)
    {
        if($dosens = Dosen::find($id)){
            $dosens->delete($id);
                return redirect('/Dosen');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function adddosen(Request $request, Dosen $dosen)
    {
        $this->validate($request, [
            'nidn'  =>'required',
            'nama_dosen'     =>'required',
            'gelar_dosen'      =>'required',
            'jenis_kelamin'   =>'required',
            'tempat_lahir'  =>'required',
            'tgl_lahir'     =>'required',
            'agama'      =>'required',
            'telepon'   =>'required',
            'alamat'  =>'required',
            'email'     =>'required',
            'foto'      =>'required',
            'password'   =>'required',
        ]);

        $dosen = $dosen->create([
            'nidn'  =>$request->nidn,
            'nama_dosen'     =>$request->nama_dosen,
            'gelar_dosen'      =>$request->gelar_dosen,
            'jenis_kelamin'   =>$request->jenis_kelamin,
            'tempat_lahir'  =>$request->tempat_lahir,
            'tgl_lahir'     =>$request->tgl_lahir,
            'agama'      =>$request->agama,
            'email'   =>$request->email,
            'alamat'  =>$request->alamat,
            'telepon'     =>$request->telepon,
            'foto'      =>$request->foto,
            'password'   =>$request->password,
        ]);

        $response = fractal()
            ->item($dosen)
            ->transformWith(new DosenTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function dosen(Dosen $dosen)
    {
        $dosens = $dosen->paginate(5);

        return fractal()
            ->collection($dosens)
            ->transformWith(new DosenTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function dosenById($id)
    {
        $response =  fractal()
            ->item(Dosen::find($id))
            ->transformWith(new DosenTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatedosen(Request $request, Dosen $dosen)
    {
        // if (News::find($news)) {
            $dosen->nidn = $request->get('nidn', $dosen->nidn);
            $dosen->nama_dosen = $request->get('nama_dosen', $dosen->nama_dosen);
            $dosen->gelar_dosen = $request->get('gelar_dosen', $dosen->gelar_dosen);
            $dosen->jenis_kelamin = $request->get('jenis_kelamin', $dosen->jenis_kelamin);
            $dosen->tempat_lahir = $request->get('tempat_lahir', $dosen->tempat_lahir);
            $dosen->tgl_lahir = $request->get('tgl_lahir', $dosen->tgl_lahir);
            $dosen->agama = $request->get('agama', $dosen->agama);
            $dosen->email = $request->get('email', $dosen->email);
            $dosen->alamat = $request->get('alamat', $dosen->alamat);
            $dosen->telepon = $request->get('telepon', $dosen->telepon);
            $dosen->foto = $request->get('foto', $dosen->foto);
            $dosen->password = $request->get('password', $dosen->password);
            $dosen->save();

            return fractal()
                ->item($dosen)
                ->transformWith(new DosenTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletedosen(Dosen $dosen)
    {
        if ($dosens = Dosen::find($dosen)) {
            $dosen->delete();
            return response()->json([
                'Message' => 'Dosen deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
