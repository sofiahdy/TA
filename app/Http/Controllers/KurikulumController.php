<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kurikulum;
use App\Transformers\KurikulumTransformer;

class KurikulumController extends Controller
{
    public function index(){
    	$kurikulums = Kurikulum::all();

    	return view ('Kurikulum.index',compact('kurikulums'));
    }

    public function create()
    {
        return view('Kurikulum.add');
    }

    public function store(Request $request)
    {
        $kurikulums = new Kurikulum();
            $kurikulums->kode_kurikulum = $request->kode_kurikulum;
            $kurikulums->kode_fakultas = $request->kode_fakultas;
            $kurikulums->kode_matakuliah = $request->kode_matakuliah;
            $kurikulums->nidn = $request->nidn;
            $kurikulums->kode_thn_ajaran = $request->kode_thn_ajaran;
            $kurikulums->kode_kelas = $request->kode_kelas;
    
        if($kurikulums->save())
            return redirect('/Kurikulum');
    }

    public function edit($id)
    {
        $kurikulums = Kurikulum::find($id);
        return view('Kurikulum.update', compact('kurikulums'));

    }

    // public function update(Request $request,$id)
    // {
    //     $kurikulum = Kurikulum::findOrFail($id);
    //     $kurikulum->fill($request->input());
        
    //     if($request->hasFile('foto'))
    //     {
    //         $file           = $request->file('foto');
    //         $fileName       = $file->getClientOriginalName();
    //         $request->file('foto')->move("image/", $fileName);
    //         $kurikulum->foto     = $fileName;
    //         $kurikulum->password  = $request->password;
    //     }
    //     $kurikulum->update();
    //     return redirect('/Kurikulum');
    // }

    public function delete($id)
    {
        if($kurikulums = Kurikulum::find($id)){
            $kurikulums->delete($id);
                return redirect('/Kurikulum');
        }else{
            return "Failed to deleted";
        }
    }

     //API
     public function addkurikulum(Request $request, Kurikulum $kurikulum)
     {
         $this->validate($request, [
             'kode_kurikulum'  =>'required',
             'kode_matakuliah'     =>'required',
             'nidn'      =>'required',
             'kode_thn_ajaran'  =>'required',
             'kode_kelas'  =>'required',
         ]);
 
         $kurikulum = $kurikulum->create([
             'kode_kurikulum'  =>$request->kode_kurikulum,
             'kode_matakuliah'     =>$request->kode_matakuliah,
             'nidn'      =>$request->nidn,
             'kode_thn_ajaran'  =>$request->kode_thn_ajaran,
             'kode_kelas'  =>$request->kode_kelas,
         ]);
 
         $response = fractal()
             ->item($kurikulum)
             ->transformWith(new KurikulumTransformer)
             ->toArray();
         return response()->json($response, 201);
     }

     public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
