<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArsipNilai;
use App\Transformers\ArsipnilaiTransformer;

class ArsipnilaiController extends Controller
{
    public function index(){
    	$arsipnilais = ArsipNilai::all();

    	return view ('ArsipNilai.index',compact('arsipnilais'));
    }

    public function create()
    {
        return view('ArsipNilai.add');
    }

    public function store(Request $request)
    {
        $arsipnilais = new ArsipNilai();
            $arsipnilais->kode_nilai = $request->kode_nilai;
            $arsipnilais->kode_kurikulum = $request->kode_kurikulum;
            $arsipnilais->kode_jenis_ujian = $request->kode_jenis_ujian;
            $arsipnilais->kode_matakuliah = $request->kode_matakuliah;
            $arsipnilais->nidn = $request->nidn;
            $arsipnilais->kode_fakultas = $request->kode_fakultas;
            $arsipnilais->kode_prodi = $request->kode_prodi;

            $file       = $request->file('foto');
            $fileName   = $file->getClientOriginalName();
            $request->file('foto')->move("image/", $fileName);
            $arsipnilais->nilai = $fileName;
    
        if($arsipnilais->save())
            return redirect('/ArsipNilai');
    }

    //API
    public function addnilai(Request $request, ArsipNilai $nilai)
    {
        $this->validate($request, [
            'kode_nilai'  =>'required',
            'kode_kurikulum'     =>'required',
            'kode_jenis_ujian'  =>'required',
            'kode_matakuliah'      =>'required',
            'nidn'      =>'required',
            'kode_fakultas'      =>'required',
            'kode_prodi'      =>'required',
            'nilai'      =>'required',
        ]);

        $nilai = $nilai->create([
            'kode_nilai'  =>$request->kode_nilai,
            'kode_kurikulum'     =>$request->kode_kurikulum,
            'kode_jenis_ujian'  =>$request->kode_jenis_ujian,
            'kode_matakuliah'      =>$request->kode_matakuliah,
            'nidn'      =>$request->nidn,
            'kode_fakultas'      =>$request->kode_fakultas,
            'kode_prodi'      =>$request->kode_prodi,
            'nilai'      =>$request->nilai,
        ]);

        $response = fractal()
            ->item($nilai)
            ->transformWith(new ArsipnilaiTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
