<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TahunAjaran;
use App\Transformers\TahunajaranTransformer;

class TahunajaranController extends Controller
{
    public function index(){
    	$tahunajarans = TahunAjaran::all();

    	return view ('TahunAjaran.index',compact('tahunajarans'));
    }

    public function create()
    {
        return view('TahunAjaran.add');
    }

    public function store(Request $request)
    {
        $tahunajarans = new TahunAjaran();
            $tahunajarans->kode_thn_ajaran = $request->kode_thn_ajaran;
            $tahunajarans->tahun = $request->tahun;
            $tahunajarans->semester = $request->semester;
    
        if($tahunajarans->save())
            return redirect('/TahunAjaran');
    }

    public function edit($id)
    {
        $tahunajarans = TahunAjaran::find($id);
        return view('TahunAjaran.update', compact('tahunajarans'));

    }

    public function update(Request $request,$id)
    {
        $tahunajaran = TahunAjaran::findOrFail($id);
        $tahunajaran->fill($request->input());
        
        $tahunajaran->update();
        return redirect('/TahunAjaran');
    }

    public function delete($id)
    {
        if($tahunajarans = TahunAjaran::find($id)){
            $tahunajarans->delete($id);
                return redirect('/TahunAjaran');
        }else{
            return "Failed to deleted";
        }
    }

    //API
    public function addtahunajaran(Request $request, TahunAjaran $tahunajaran)
    {
        $this->validate($request, [
            'kode_thn_ajaran'  =>'required',
            'tahun'     =>'required',
            'semester'      =>'required',
        ]);

        $tahunajaran = $tahunajaran->create([
            'kode_thn_ajaran'  =>$request->kode_thn_ajaran,
            'tahun'     =>$request->tahun,
            'semester'      =>$request->semester,
        ]);

        $response = fractal()
            ->item($tahunajaran)
            ->transformWith(new TahunajaranTransformer)
            ->toArray();
        return response()->json($response, 201);
    }

    public function news(News $news)
    {
        $newses = $news->paginate(5);

        return fractal()
            ->collection($newses)
            ->transformWith(new NewsTransformer)
            ->toArray();
        // return \App\Http\Resources\NewsResource::collection($newses);
    }

    public function newsById($id)
    {
        $response =  fractal()
            ->item(News::find($id))
            ->transformWith(new NewsTransformer)
            ->toArray();

        return response()->json($response,200);
    }

    public function updatenews(Request $request, News $news)
    {
        // if (News::find($news)) {
            $news->category = $request->get('category', $news->category);
            $news->title = $request->get('title', $news->title);
            $news->news = $request->get('news', $news->news);
            $news->content = $request->get('content', $news->content);
            $news->save();

            return fractal()
                ->item($news)
                ->transformWith(new NewsTransformer)
                ->toArray();
            // return response()->json($response);
        // }else{
        //     return response()->json([
        //         'Message' => 'Failed to update',
        //     ]);
        // }
        
    }

    public function deletenews(News $news)
    {
        if ($newses = News::find($news)) {
            $news->delete();
            return response()->json([
                'Message' => 'News deleted',
            ]);
        }else{
            return response()->json([
                'Message' => 'Failed to delete',
            ]);
        }
        
    }
}
