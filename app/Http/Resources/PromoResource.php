<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PromoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id'                => $this->id,
            'promo_image'       => $this->promo_image,
            'short_description' => $this->short_description,
            'detail'            => $this->detail,
        ];
    }
}
