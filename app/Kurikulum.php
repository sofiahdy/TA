<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurikulum extends Model
{
    protected $table='kurikulum';
    protected $fillable = [
        'kode_kurikulum', 'kode_fakultas', 'kode_matakuliah', 'nidn', 'kode_thn_ajaran', 'kode_kelas',
    ];
}
