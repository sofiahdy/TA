<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArsipSoal extends Model
{
    protected $table='soal';
    protected $fillable = [
        'kode_soal', 'kode_kurikulum', 'kode_jenis_ujian', 'kode_matakuliah', 'nidn', 'kode_fakultas', 'kode_prodi', 'soal',
    ];
}
